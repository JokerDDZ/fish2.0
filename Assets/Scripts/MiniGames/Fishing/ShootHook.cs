﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootHook : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject hook;
    public GameObject progressBar;

    private GameObject rope;

    public GameObject littleFish;
    public GameObject Fish;
    public GameObject BigFish;

    public int whatFish = 0;
    public float power = 0f;
    private float speedPower = 1000f;
    private const float maxPower = 2000f;
    private float timeAfterWeGetFish = 0f;
    private Vector3 offsetBeetweenHookAndFish = new Vector3(-0.59f, 1.593f, 0.17f);

    public bool space = true;

    private void Start()
    {
        rope = GameObject.Find("RopeNoPhysic");
    }

    private void Update()
    {
        if(space == true)
        {
            if (Input.GetMouseButton(0))
            {
                GetComponent<Animator>().SetBool("LoadHook", true);

                power += speedPower * Time.deltaTime;

                if (power >= maxPower)
                {
                    power = maxPower;
                }

                progressBar.GetComponent<Image>().fillAmount = power / maxPower;
            }

            if (Input.GetMouseButtonUp(0))
            {
                space = false;
                GetComponent<Animator>().SetBool("ThrowHook", true);
            }
        }    
    }

    public void shootHook()
    {
        rope.GetComponent<RopeScript>().ena = true;
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;

        hook.transform.parent = null;
        rb.AddForce((rb.transform.forward + (Vector3.up * 0.15f))  * power);

        timeAfterWeGetFish = Random.Range(2f, 6f);

        progressBar.transform.parent.gameObject.SetActive(false);

        hook.GetComponent<HookFreezeRotation>().enabled = false;

        StartCoroutine(weGetFish());
    }

    private IEnumerator weGetFish()
    {
        yield return new WaitForSeconds(timeAfterWeGetFish);

        GetComponent<FishingRodChangeColor>().enabled = true;
        hook.transform.GetChild(1).GetComponent<Animator>().SetBool("upAndDown", true);

        whatFish = Random.Range(0, 3);

        switch(whatFish)
        {
            case 1:
                createFish(Fish, offsetBeetweenHookAndFish, Quaternion.Euler(0f, 0f, -40f));
                break;
            case 2:
                createFish(BigFish, new Vector3(-0.227f, 1.253f, -0.094f), Quaternion.Euler(-90f, 0f, 0f));
                break;
            case 0:
                createFish(littleFish, new Vector3(-0.187f, 1.982f, -0.122f), Quaternion.Euler(0f, 90f,-180f));
                break;
        }

        hook.GetComponent<HookMovement>().enabled = true;
        StartCoroutine(hook.GetComponent<HookMovement>().fishGoInRandonDir());
    }

    public void createFish(GameObject f, Vector3 offset,Quaternion q)
    {
        var p = Instantiate(f, hook.transform.position + offset, q);
        p.transform.parent = hook.transform;
    }
}
