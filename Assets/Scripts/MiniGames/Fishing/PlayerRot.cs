﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRot : MonoBehaviour
{
    private float MouseSensitivity = 2f;
    public bool goRot = false;

    Rigidbody Rigid;

    private void Start()
    {
        StartCoroutine(enableRot());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(goRot)
        {
            //Rigid.MoveRotation(Rigid.rotation * Quaternion.Euler(new Vector3(0, Input.GetAxis("Mouse X") * MouseSensitivity, 0)));
            transform.Rotate(new Vector3(0f, Input.GetAxis("Mouse X") * MouseSensitivity, 0f));
        }        
    }

    public IEnumerator enableRot()
    {
        yield return new WaitForSeconds(1f);

        goRot = true;
    }
}
