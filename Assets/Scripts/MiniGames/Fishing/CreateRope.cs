﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateRope : MonoBehaviour
{
    private LineRenderer line;

    private GameObject[] pointsy;

    public float springParametr = 1000f;
    public float damprerParametr = 0.5f;


    void Start()
    {
        pointsy = GameObject.FindGameObjectsWithTag("PointsOfLine");

        line = GetComponent<LineRenderer>();
        line.startWidth = 0.01f;
        line.endWidth = 0.01f;
        line.positionCount = 2;
        line.startColor = new Color(1f, 0f, 0f);
        line.sortingOrder = 5;
        line.positionCount = pointsy.Length;
    }

    void Update()
    {
        for (int i = 0; i < pointsy.Length; i++)
        {
            line.SetPosition(i, pointsy[i].transform.position);
            pointsy[i].GetComponent<SpringJoint>().spring = springParametr;
            pointsy[i].GetComponent<SpringJoint>().damper = damprerParametr;
        }
    }
}
