﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookFreezePosition : MonoBehaviour
{
    private Rigidbody rb;
    

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY;
            rb.velocity = new Vector3(0f, 0f, 0f);
        }
    }

}
