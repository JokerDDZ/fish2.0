﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodChangeColor : MonoBehaviour
{
    public HookMovement hookMovement;
    public Color colorOfRod;
    public Shader s;

    private Renderer rodRenderer;

    // Start is called before the first frame update
    void Start()
    {
        rodRenderer = GameObject.Find("FishingROD").GetComponent<SkinnedMeshRenderer>();
            //transform.GetChild(0).gameObject.GetComponent<Renderer>();
         var hook = GameObject.Find("Hook");
         hookMovement = hook.GetComponent<HookMovement>();

        rodRenderer.material.SetColor("BaseColor", Color.red);
        s = rodRenderer.material.shader;
    }

    // Update is called once per frame
    void Update()
    {
        float angle = hookMovement.angleBeetweenHookAndRod;
        float scale = hookMovement.scaleForAngle;
        float red = angle / scale;

        if(red > 1)
        {
            red = 1;
        }

        colorOfRod = new Color(1f, 1f - red, 1f - red);

        Material m = new Material(s);
        m = rodRenderer.material;

        m.SetColor("_BaseColor", colorOfRod);

        rodRenderer.material = m;
    }
    
}
