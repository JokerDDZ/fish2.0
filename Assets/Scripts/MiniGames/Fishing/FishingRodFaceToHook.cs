﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodFaceToHook : MonoBehaviour
{
    private GameObject hook;

    // Start is called before the first frame update
    void Start()
    {
        hook = GameObject.Find("Float");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var lookPos = hook.transform.position - transform.position;
        var damping = 5f;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }

}
