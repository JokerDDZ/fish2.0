﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraControllerPers : MonoBehaviour
{
    private Animator am;

    private void Start()
    {
        am = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("q") == true)
        {
            if(am.GetBool("TPP") == false)
            {
                am.SetBool("TPP", true);
            }
            else
            {
                am.SetBool("TPP", false);
            }
        }
    }
}
