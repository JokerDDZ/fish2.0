﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetFishFromHook : MonoBehaviour
{
    public GameObject hook;
    public Text points;
    public GameObject floatForFish;

    private GameObject rope;

    public Image progressBar;
    public Image progressBarPowerOfFish;

    private float pointsNumber;

    Animator a;

    private void Start()
    {
        rope = GameObject.Find("RopeNoPhysic");
        a = GetComponent<Animator>();
    }

    public void restart()
    {
        GameObject player = GameObject.Find("Player");

        rope.GetComponent<LineRenderer>().startColor = Color.black;
        rope.GetComponent<LineRenderer>().endColor = Color.black;

        hook.GetComponent<HookMovement>().timer = 0f;
        hook.GetComponent<HookMovement>().timeLeft = 0f;

        progressBarPowerOfFish.GetComponent<Image>().fillAmount = 0f;

        hook.transform.rotation = new Quaternion(0f, 0f, 0f, 0);

        a.SetBool("ThrowHook", false);
        a.SetBool("LoadHook", false);
        a.SetBool("goUpFish", false);
        a.SetBool("getFish", false);

        floatForFish.GetComponent<Animator>().SetBool("upAndDown", false);

        GetComponent<ShootHook>().space = true;
        GetComponent<ShootHook>().power = 0f;
        hook.GetComponent<HookMovement>().HPOfRope = 100f;

        progressBar.transform.GetChild(0).GetComponent<Image>().fillAmount = 0f;
        progressBar.gameObject.SetActive(true);
        hook.GetComponent<HookFreezeRotation>().enabled = true;

        hook.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        hook.GetComponent<Rigidbody>().isKinematic = true;

        StartCoroutine(setOffRot());

        GetComponent<FishingRodChangeColor>().enabled = false;

        GameObject.Find("FishingROD").GetComponent<SkinnedMeshRenderer>().material.SetColor("_BaseColor", new Color(1f,1f,1f,1f));

        //rope.GetComponent<CreateRope>().destroyLine();
    }

    IEnumerator setOffRot()
    {
        yield return new WaitForSeconds(1f);
        transform.GetComponentInParent<PlayerRot>().goRot = true;
        a.SetBool("fail", false);
    }

    public void desFishAndAddPoints()
    {
        var howMuchKG = GetComponent<ShootHook>().whatFish;

        pointsNumber += 5 * (howMuchKG + 1);
        points.text = pointsNumber.ToString() + "kg";
        Destroy(hook.transform.GetChild(2).gameObject);
        restart();
    }
}
