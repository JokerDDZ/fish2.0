﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodBend : MonoBehaviour
{
    private GameObject hook;

    // Start is called before the first frame update
    void Start()
    {
        hook = GameObject.Find("Hook");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var v = hook.GetComponent<HookMovement>().velocity;

        Vector3 toGo = new Vector3(0f, 0f, (v / 10f) * 50);

        Vector3 myVec = Vector3.Lerp(transform.localEulerAngles, toGo, Time.deltaTime);


       transform.localEulerAngles = myVec;
    }
}
