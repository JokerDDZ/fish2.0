﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLineBeetweenChain : MonoBehaviour
{
    GameObject rope;
    private GameObject [] elementsOfRope;
    LineRenderer lr;

    private int children;

    // Start is called before the first frame update
    void Start()
    {
        children = transform.childCount;
        rope = GameObject.Find("Rope");
        lr = GetComponent<LineRenderer>();
        lr.startWidth = 0.05f;
        lr.endWidth = 0.05f;
        lr.startColor = Color.black;
        lr.endColor = Color.black;
        lr.positionCount = children;
    }

    // Update is called once per frame
    void Update()
    {
        

        for(int i = 0; i < children;i++)
        {
            lr.SetPosition(i, transform.GetChild(i).transform.position);
        }
    }
}
