﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeScript : MonoBehaviour
{
    public GameObject fish;
    private GameObject begin;
    private GameObject end;
    public GameObject endPointOfTheRod;

    public AnimationCurve curve;

    public float blend = 2f;
    public bool ena = false;

    private void Start()
    {
        var children = transform.childCount;
        begin = transform.GetChild(0).gameObject;
        end = transform.GetChild(children - 1).gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ena)
        {
            begin.transform.position = endPointOfTheRod.transform.position;
            end.transform.position = fish.transform.position;

            var vec = end.transform.position - begin.transform.position;

            var distForPoints = vec / (transform.childCount - 1);

            for (int i = 1; i < transform.childCount - 1; i++)
            {
                var myXZ = begin.transform.position + (i * distForPoints);

                var x = Vector3.Distance(fish.transform.position, endPointOfTheRod.transform.position);

                var dis = Vector3.Distance(transform.GetChild(i).transform.position, fish.transform.position);
                var myX = dis/x;

                //Debug.Log("X: " + x);
                //Debug.Log("Dis: " + dis);
                //Debug.Log("MyX: " + myX);
                var y = curve.Evaluate(myX);
               // Debug.Log(y);

                //if(myX > 0 && myX < 1)
                // {
                transform.GetChild(i).transform.position = myXZ + (Vector3.down * blend * y);
                //}

                // transform.GetChild(i).transform.position = myXZ;

            }

        }
        else
        {
            begin.transform.position = endPointOfTheRod.transform.position;
            end.transform.position = fish.transform.position;

            var vec = end.transform.position - begin.transform.position;

            var distForPoints = vec / (transform.childCount - 1);

            for (int i = 1; i < transform.childCount - 1; i++)
            {

                var myXZ = begin.transform.position + (i * distForPoints);

                var x = Vector3.Distance(fish.transform.position, endPointOfTheRod.transform.position);

                var dis = Vector3.Distance(transform.GetChild(i).transform.position, fish.transform.position);
                var myX = dis / x;

                transform.GetChild(i).transform.position = myXZ;

            }
        }
    }

    public void downCurve()
    {
        blend += Time.deltaTime;

        if(blend > 1)
        {
            blend = 1;
        }
    }

    public void upCurve()
    {
        blend -= Time.deltaTime;

        if(blend < 0)
        {
            blend = 0;
        }
    }
}
